angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })
  .state('app.home', {
    url: "/home",
    views: {
      'menuContent': {
        templateUrl: "templates/home.html",
    controller: 'AppCtrl'
      }
    }
  })
  .state('app.settings', {
    url: "/settings",
    views: {
      'menuContent': {
        templateUrl: "templates/settings.html",
    controller: 'SettingsCtrl'
      }
    }
  })
  .state('app.fav', {
    url: "/fav",
    views: {
      'menuContent': {
        templateUrl: "templates/fav.html",
    controller: 'FavCtrl'
      }
    }
  })
  .state('app.map', {
    url: "/map",
    views: {
      'menuContent': {
        templateUrl: "templates/map.html",
        controller: 'MapCtrl'
      }
    }
  })
  .state('app.detail', {
    url: "/detail/:id",
    views: {
      'menuContent': {
        templateUrl: "templates/detail.html",
        controller: 'DetailCtrl'
      }
    }
  })
  .state('app.detail2', {
    url: "/detail2/:id",
    views: {
      'menuContent': {
        templateUrl: "templates/detail2.html",
        controller: 'Detail2Ctrl'
      }
    }
  })
  .state('app.transport', {
    url: "/transport/:latitude/:longitude",
    views: {
      'menuContent': {
        templateUrl: "templates/transport.html",
        controller: 'TransportCtrl'
      }
    }
  })
  .state('app.comment', {
    url: "/comment/:id",
    views: {
      'menuContent': {
        templateUrl: "templates/comment.html",
        controller: 'CommentCtrl'
      }
    }
  })
  .state('app.commentRead', {
    url: "/commentRead/:id",
    views: {
      'menuContent': {
        templateUrl: "templates/commentRead.html",
        controller: 'CommentReadCtrl'
      }
    }
  })
  .state('app.commentPost', {
    url: "/commentPost/:id",
    views: {
      'menuContent': {
        templateUrl: "templates/commentPost.html",
        controller: 'CommentPostCtrl'
      }
    }
  })
  $urlRouterProvider.otherwise('/app/home');
});

angular.module('starter.controllers', ['ngCordova', 'ngAnimate', 'ionic', 'LocalStorageModule'])

.controller('AppCtrl', function($scope, $rootScope, $cordovaDevice, localStorageService) {
    
    //         0 - EXTERNE PUBLIC               1 - CETIC LOCAL              2 - VAGRANT LOCAL
    var ip = ['http://pegase.cetic.be:63009' , 'http://172.23.101.39:8000', 'http://vagrant:8000'];
    
    $rootScope.refresh = 0;
    $rootScope.favRefresh = 0;
    var debug = 0;
    $rootScope.ipServer = ip[0];
    document.addEventListener("deviceready", function () {
        var uuid = $cordovaDevice.getUUID();
        $rootScope.uuid = uuid;
    }, false);
    
    $scope.profil = localStorageService.get('profil');
    if ($scope.profil == null){   
        $scope.profil = [
            {handi: "1", content: "Chaise roulante", checked: true},
            {handi: "2", content: "Marchant difficilement", checked: true},
            {handi: "3", content: "Aveugle", checked: true},
            {handi: "4", content: "Malvoyant", checked: true},
            {handi: "5", content: "Sourd", checked: true},
            {handi: "6", content: "Malentendant", checked: true},
            {handi: "7", content: "Difficulté de compréhension", checked: true}
        ];
        localStorageService.set('profil', $scope.profil);
    }
    var fav = localStorageService.get('fav');
    if (fav == null)
    {
        fav = [];
        localStorageService.set('fav', fav);
    }
})
.controller('MapCtrl', function($scope, $rootScope, $http){
    /*
    INITIALISATION MAP
    ====================================================
    */
    var mapOptions = {
        center: {lat: 50.854975, lng: 4.3753899},
        zoom: 10
		
    };
    map = new google.maps.Map(document.getElementById('map-canvas'),
            mapOptions);
    /*
    GET ACTUAL POSITION - GPS ? + SWITCH VIEW TO THIS COORD
    ====================================================
    */
    if(navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

			var dataCircle = {
				strokeColor: '#4183D7',
				strokeOpacity: 0.4,
				strokeWeight: 2,
				fillColor: '#59ABE3',
				fillOpacity: 0.2,
				map: map,
				center: pos,
				radius: 80
			};
			locationCircle = new google.maps.Circle(dataCircle);
			
			google.maps.event.addListener(locationCircle, 'click', function() { 
				locationCircle.setMap(null);
			});
			
			map.setCenter(pos);
		}, function() {
			handleNoGeolocation(true);
		});
	} else {
		handleNoGeolocation(false);
	}
    /*
    ADD MARKER ACCESSI + LABEL
    ====================================================
    */
    $http.get($rootScope.ipServer+"/btmnt/")
    .success(function(response) {
        angular.forEach(response, function(value, key) {
            var myLatlng = new google.maps.LatLng(parseFloat(value["latitude"]), parseFloat(value["longitude"]));
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                title:  ""
            });
            var infowindow = new google.maps.InfoWindow({
                content: "<b>Nom</b>: "+value["titre"]
                +" <br /><b>Categorie</b>:"+value["categorie"]
                +"<br /><b>Téléphone</b>:"+value["telephone"]
                +"<br /><b><a href='#/app/detail/"+value["id_batiment"]+"'>Voir plus</a>"
            });
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map,marker);
            });
        });
    })
    .error(function(err){
       alert(err); 
    });
    $http.get($rootScope.ipServer+"/jaccede/poi?where=liege+belgique")
    .success(function(response) {
        angular.forEach(response, function(value, key){
            var myLatlng = new google.maps.LatLng(parseFloat(value["latitude"]), parseFloat(value["longitude"]));
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png',
                title:  ""
            });
            var infowindow = new google.maps.InfoWindow({
                content: "<b>Nom</b>: "+value["name"]
                +" <br /><b>Categorie</b>:"+value["category"]
                +"<br /><b>Téléphone</b>:"+value["telephone"]
                +"<br /><b><a href='#/app/detail2/"+value["uid"]+"'>Voir plus</a>"
            });
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map,marker);
            });
        });
    })
    .error(function(err){
       alert(err); 
    });
})
.controller('FavCtrl', function($scope, $rootScope, $http, $state, localStorageService){
    $rootScope.$on('$locationChangeSuccess', function () {
        if($rootScope.favRefresh == 1){
            $rootScope.favRefresh = 0;
            loadData();
        }
    });
     function loadData(){
        $scope.objEmpty = false;
        var obj = localStorageService.get('fav');
        $scope.data = obj;
        if ($scope.data.length == 0) $scope.objEmpty = true;
    }
    loadData();
    $scope.goDetail = function(item){
        var id = item.id;
        var tmp = id.split('_');
        if(tmp[0] == "ACCESSI"){
            $state.go('app.detail', {'id': tmp[1]});
        }else{
            $state.go('app.detail2', {'id': tmp[1]});
        }
    }
})
.controller('DetailCtrl', function($scope, $rootScope, $http, $location, $ionicModal, $state, localStorageService){

    $scope.listPhoto = [];
    $scope.commentCount = "...";
    $scope.modalText = "Chargement ...";
    $scope.handiVide = false;
    $scope.isFav = false;
    $scope.id = $location.path().split('/')[3];
    
    var favList = localStorageService.get('fav');
    angular.forEach(favList, function(value, key){
        if(value.id == 'ACCESSI_'+$scope.id){
            $scope.isFav = true;
        }
    });
    
    $scope.parserColor = [
        {in: "Vert", out: "green"},
        {in: "Orange", out: "orange"},
        {in: "Gris", out: "gray"}
    ]; 
    $scope.profil = localStorageService.get('profil');
    $scope.handi = [
        {
            handi: "1",
            rest: "lvl_chaise_roulante",
            plus: "text_plus_chaise_roulante",
            moins: "text_moins_chaise_roulante",
            content: "Chaise roulante",
            color: "",
            show: true
        },
        {
            handi: "2",
            rest: "lvl_marchant_difficilement",
            plus: "text_plus_marchant_difficilement",
            moins: "text_moins_marchant_difficilement",
            content: "Marchant difficilement",
            color: "",
            show: true
        },
        {
            handi: "3",
            rest: "lvl_aveugle",
            plus: "text_plus_aveugle",
            moins: "text_moins_aveugle",
            content: "Aveugle",
            color: "",
            show: true
        },
        {
            handi: "4",
            rest: "lvl_malvoyante",
            plus: "text_plus_malvoyante",
            moins: "text_moins_malvoyante",
            content: "Malvoyant",
            color: "",
            show: true
        },
        {
            handi: "5",
            rest: "lvl_sourde",
            plus: "text_plus_sourde",
            moins: "text_moins_sourde",
            content: "Sourd",
            color: "",
            show: true
        },
        {
            handi: "6",
            rest: "lvl_malentendante",
            plus: "text_plus_malentendante",
            moins: "text_moins_malentendante",
            content: "Malentendant",
            color: "",
            show: true
        },
        {
            handi: "7",
            rest: "lvl_difficulte_comprehension",
            plus: "text_plus_difficulte_comprehension",
            moins: "text_moins_difficulte_comprehension",
            content: "Difficulté de compréhension",
            color: "",
            show: true
        }
    ];
    $http.get($rootScope.ipServer+"/batiment/"+$scope.id+"/")
    .success(function(response) {
        $scope.batiment = response;
        $scope.listPhoto = response["images"].split(';');
        $http.get($scope.batiment["fk_label"])
        .success(function(response) {
            $scope.label = response;
            angular.forEach(response, function(value, key) {
                angular.forEach($scope.handi, function(value2, key2) { 
                    if(value2.rest == key)
                    {
                        angular.forEach($scope.parserColor, function(value3, key3) {
                            if(value3.in == value){
                             value2.color = value3.out;   
                            }
                        });
                    }
                });
            });
        })
        .error(function(err){
            alert("label"+ err);
        });
        $http.get($scope.batiment["fk_contact"])
            .success(function(response) {
        $scope.contact = response;
        })
        .error(function(err){
            alert("contact"+ err);
        });
        $http.get($rootScope.ipServer+'/comment/count?id=ACCESSI_'+$scope.batiment["id_batiment"])
        .success(function(response){
            $scope.commentCount = response.count;
        })
        .error(function(errno){
            alert(errno);
        });
        var i = 0;
        var count = 0;
        angular.forEach($scope.handi, function(value, key){
            if($scope.profil[i].checked == true){
               count = count + 1;
            }
            value.show = $scope.profil[i].checked;
            i = i + 1;
        });
        if(count == 0){
            $scope.handiVide = true;
        }
    })
    .error(function(err){
            alert("batiment"+ err);
    });
    $ionicModal.fromTemplateUrl('detail-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.openModal = function(type_plus, type_moins) {
        angular.forEach($scope.label, function(value, key){
            if(key == type_plus){
                $scope.modalPlus = value;
                console.log($scope.modalPlus);
            }
            if(key == type_moins){
                $scope.modalMoins = value;
            }
        });
        
        $scope.modal.show();
    };
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
    $scope.$on('$destroy', function() {
        $scope.modal.remove();
    });
    $scope.$on('modal.hidden', function() {
        // Execute action
    });
    $scope.$on('modal.removed', function() {
        // Execute action
    });
    $scope.urlTransport = function(){
          $state.go('app.transport',{'latitude': parseFloat($scope.batiment["latitude"]), 'longitude': parseFloat($scope.batiment["longitude"])});
    };
    $scope.urlComment = function(){
        $state.go('app.comment',{'id': 'ACCESSI_'+$scope.batiment["id_batiment"]});
    }
    
    
    $scope.addToFav = function(){
        try{
        if($scope.isFav){
            var pos = 0;
            var found = -1;
            angular.forEach(favList, function(value, key){
                if('ACCESSI_'+value.id == $scope.id){
                    found = pos;
                }
                pos = pos + 1;
            });
            if(found != -1){
                favList.splice(found,1);
                alert($scope.batiment.titre + " a été retiré de vos favoris");
                $scope.isFav = false;
                $rootScope.favRefresh = 1;
                localStorageService.set('fav', favList);
            }
        }else{
            favList.push({id: 'ACCESSI_'+$scope.batiment.id_batiment, titre: $scope.batiment.titre, ville: $scope.batiment.ville});
            localStorageService.set('fav', favList);
            alert($scope.batiment.titre + " a été ajouté à vos favoris");
            $scope.isFav = true;
            $rootScope.favRefresh = 1;
        }
        }catch(e){
            alert(e);
        }
    }
})
.controller('Detail2Ctrl', function($scope, $rootScope, $http, $location, $state, localStorageService){
    $scope.id = $location.path().split('/')[3];
    $scope.loadingScreen = true;
    $scope.dataScreen = false;
    $scope.btmTitle = "Chargement";
    $scope.commentCount = "...";
    

    var favList = localStorageService.get('fav');
    angular.forEach(favList, function(value, key){
        if(value.id == "JACCEDE_"+$scope.id){
            $scope.isFav = true;
        }
    });
    
    $http.get($rootScope.ipServer+"/jaccede/detail?uid="+$scope.id)
    .success(function(response){
        $scope.loadingScreen = false;
        $scope.dataScreen = true;
        $scope.batiment = response;
        $scope.btmTitle = response["name"];
    })
    .error(function(errno){
        alert(errno);
    });
    

    $http.get($rootScope.ipServer+'/comment/count?id=JACCEDE_'+$scope.id)
    .success(function(response){
        $scope.commentCount = response.count;
    })
    .error(function(errno){
        alert(errno);
    });
    $scope.urlTransport = function(){
          $state.go('app.transport',{'latitude': parseFloat($scope.batiment.latitude), 'longitude': parseFloat($scope.batiment.longitude)});
    };
    $scope.urlComment = function(){
        $state.go('app.comment',{'id': 'JACCEDE_'+$scope.id});
    }

    $scope.addToFav = function(){
        try{
        if($scope.isFav){
            var pos = 0;
            var found = -1;
            angular.forEach(favList, function(value, key){
                if('JACCEDE_'+value.id == $scope.id){
                    found = pos;
                }
                pos = pos + 1;
            });
            if(found != -1){
                favList.splice(found,1);
                alert($scope.batiment.titre + " a été retiré de vos favoris");
                $scope.isFav = false;
                $rootScope.favRefresh = 1;
                localStorageService.set('fav', favList);
            }
        }else{
            favList.push({id: 'JACCEDE_'+$scope.id, titre: $scope.batiment.name, ville: $scope.batiment.ville});
            localStorageService.set('fav', favList);
            alert($scope.batiment.titre + " a été ajouté à vos favoris");
            $scope.isFav = true;
            $rootScope.favRefresh = 1;
        }
        }catch(e){
            alert(e);
        }
    }
})
.controller('TransportCtrl', function($scope, $rootScope, $http, $location, $state){
    $scope.busplus = true;
    $scope.radiusBus = 200;
    $scope.radiusTrain = 3000;
    $scope.latitude = $location.path().split('/')[3];
    $scope.longitude = $location.path().split('/')[4];
    getBus(0);
    getTrain(0);
    
    $scope.addBus = function(){
        $scope.busData = true;
        $scope.busloading = false;
        getBus(200);
    }
    $scope.addTrain = function(){
        $scope.trainData = true;
        $scope.trainloading = false;
        getTrain(500);
    }
    
    function getBus(add){
       $scope.radiusBus = $scope.radiusBus + parseInt(add);
        $http.get($rootScope.ipServer+'/tec/find?latitude='+$scope.latitude+'&longitude='+$scope.longitude+'&radius='+$scope.radiusBus)
        .success(function(response){
            $scope.bus = response;
            $scope.busData = false;
            $scope.busloading = true;
            $scope.busplus = false;
        })
        .error(function(errno){
           alert(errno); 
        });
    }
    function getTrain(add){
       $scope.radiusTrain = $scope.radiusTrain + parseInt(add); $http.get($rootScope.ipServer+'/train/find?latitude='+$scope.latitude+'&longitude='+$scope.longitude+'&radius='+$scope.radiusTrain)
        .success(function(response){
            $scope.train = response;
            $scope.trainData = false;
            $scope.trainloading = true;
            $scope.trainplus = false;
        })
        .error(function(errno){
           alert(errno); 
        });
    }
})
.controller('CommentCtrl', function($scope, $rootScope, $http, $location, $state, $ionicSlideBoxDelegate){
   $ionicSlideBoxDelegate.update();
    $rootScope.$on('$locationChangeSuccess', function () {
        if($rootScope.refresh == 1){
            $rootScope.refresh = 0;
            loadData();
        }
    });
    $scope.orderItem = [{isOrder : 0, val : 'fields.rate'}, {isOrder : 0, val : 'fields.timestamp'}];
    $scope.id = $location.path().split('/')[3];
    function loadData(){
        $scope.dataUrl = $rootScope.ipServer+'/comment/getList?id='+$scope.id;
        $http.get($scope.dataUrl)
        .success(function(response){
            $scope.comments = response; 
        })
        .error(function(data, status){
            $scope.error = data;
            alert(status);
        });
    }
    loadData();
    $scope.newComment = function(){
       $state.go('app.commentPost', {'id': $scope.id});
    }
    $scope.readComment = function(id){
        $state.go('app.commentRead', {'id': id});
    }
    $scope.refreshComment = function(){
        loadData();
    }
    $scope.orderComment = function(para){
        $scope.orderItem[para].isOrder = $scope.orderItem[para].isOrder == 0 ? 1 : 0;
        var cara = $scope.orderItem[para].isOrder == 1 ? '+' : '-';
        $scope.orderList = cara + $scope.orderItem[para].val;
    }
})
.controller('CommentPostCtrl', function($scope, $rootScope, $http, $location, $state, localStorageService){
    var name = localStorageService.get('name');
    var mail = localStorageService.get('mail');
    $scope.comment = {name: name, mail: mail};
    
    var timeMilli = +new Date;
    $scope.id = $location.path().split('/')[3];
    $scope.sendComment = function(obj){
        $http.post($rootScope.ipServer+'/comment/', {
            title: obj.title,
            content: obj.content,
            timestamp: parseInt(timeMilli/1000),
            name: obj.name,
            mail: obj.mail,
            unique: $rootScope.uuid,
            status: 1,
            parent: $scope.id,
            rate : 0
        }).success(function(data, status, headers, config) {
            $rootScope.refresh = 1;
            $state.go('app.comment', {'id': $scope.id});
        }).error(function(data, status, headers, config) {
            alert(status);
        });
    }
    $scope.backReload = function(){
        $rootScope.refresh = 1;
        $state.go('app.comment', {'id': $scope.id});
    }
})
.controller('CommentReadCtrl', function($scope, $rootScope, $http, $location, $state){
    
    $scope.alreadyRated = 0;
    $scope.id = $location.path().split('/')[3];
    $http.get($rootScope.ipServer+'/comment/'+$scope.id)
    .success(function(response){
        $scope.comment = response;
        $http.get($rootScope.ipServer+'/comment/isRated?id='+$scope.id+'&client='+$rootScope.uuid)
        .success(function(response){
            $scope.rated = response;
            if(response.isRated){
                $scope.alreadyRated = response.rate;   
            }
        })
        .error(function(errno){
            alert(errno);
        });
    })
    .error(function(errno){
       alert(errno); 
    });
    $scope.rateP = function(){
        if ($scope.alreadyRated == 0){
            $http.post($rootScope.ipServer+'/rate/', {
                parent: $scope.id,
                client: $rootScope.uuid,
                rate: 1
            }).success(function(data, status, headers, config) {
                $scope.alreadyRated = 1;
            }).error(function(data, status, headers, config) {
                alert(status);
            });
        }else{
            alert("Tu as déjà voté");
        }
    }
    $scope.rateN = function(){
        if ($scope.alreadyRated == 0){
            $http.post($rootScope.ipServer+'/rate/', {
                parent: $scope.id,
                client: $rootScope.uuid,
                rate: -1
            }).success(function(data, status, headers, config) {
                $scope.alreadyRated = -1;
            }).error(function(data, status, headers, config) {
                alert(status);
            });
        }else{
            alert("Tu as déjà voté");
        }
    }
    $scope.report = function(){
        alert('Ce commentaire a été signaler !');
        //alert($scope.id);
    }
    $scope.backComment = function(){
        $rootScope.refresh = 1;
        $state.go('app.comment', {'id': $scope.comment["parent"]});
    }
})
.controller('SettingsCtrl', function($scope, $rootScope, localStorageService){
    try{
        var name = localStorageService.get('name');
        var mail = localStorageService.get('mail');
        $scope.client = {name: name, mail: mail};
        $scope.profil = localStorageService.get('profil');
    }catch(e){
         alert(e);   
    }
    $scope.saveData = function(client){
        localStorageService.set('name', client.name);
        localStorageService.set('mail', client.mail);
        localStorageService.set('profil', $scope.profil);
        alert('Profil mis a jour');
    }
    $scope.deleteData = function(client){
        $scope.client = {name: '', mail: ''};
        localStorageService.set('name', '');
        localStorageService.set('mail', '');
        $rootScope.favRefresh = 1;
        localStorageService.set('fav', []);
        $scope.profil = [
            {handi: "1", content: "Chaise roulante", checked: true},
            {handi: "2", content: "Marchant difficilement", checked: true},
            {handi: "3", content: "Aveugle", checked: true},
            {handi: "4", content: "Malvoyant", checked: true},
            {handi: "5", content: "Sourd", checked: true},
            {handi: "6", content: "Malentendant", checked: true},
            {handi: "7", content: "Difficulté de compréhension", checked: true}
        ];
        localStorageService.set('profil', $scope.profil);
        alert('Le profil a été remis a zéro');
    }
});